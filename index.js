var express = require('express');
var app = express();
var https = require('https');

//incorporo los directorios en manera estática, requiero las imágenes, por eso uso express
app.use(express.static(__dirname + '/app'));
app.use(express.static(__dirname + '/app/banderas'));
app.use(express.static(__dirname + '/app/images'));


app.get('/',function(req,res){
  res.sendFile('index.html');//va a buscar este archivo en algún directorio anterior
});

app.listen(3000);

console.log("Running at Port 3000");
