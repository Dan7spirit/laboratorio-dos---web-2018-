

$().ready(function(){
	$("#btnSaveChanges").click(function () {
		checkFavorites();
	});
	$("#btnConvert").click(function (){
		convertCurrencies();
	});
});

function checkFavorites(){
	var elements = document.getElementById("formAddFCurrencies").elements;
	for(var i = 0, element; element = elements[i++];){
		if(element.type ==="checkbox"){
			var id_f_li = element.id;//identificador favorite list item
			id_f_li = id_f_li.substring(0, id_f_li.length - 1);
			id_f_li += "f";
			if(element.checked){
				if(document.getElementById(id_f_li).className === 'hide'){
					document.getElementById(id_f_li).classList.remove('hide');
					document.getElementById(id_f_li).classList.add('show');
				}
			}else{
				if(document.getElementById(id_f_li).className === 'show'){
					document.getElementById(id_f_li).classList.remove('show');
					document.getElementById(id_f_li).classList.add('hide');
				}
			}
		}
	}
	return false;
};

function clickOnInputText(callingInput){
	var callingLabel = callingInput.id;
	callingLabel = callingLabel += "-con";
	document.getElementById(callingLabel).innerHTML ="";
	var elements = document.getElementById("formFavorites_currencies").elements;
	for(var i = 0, element; element = elements[i++];){
		if(element.id !== callingInput.id){
			if(element.value !== ""){
				element.value = "";
			}
		}
	}
}

function convertCurrency(amount, fromCurrency, toCurrency) {

	fromCurrency = encodeURIComponent(fromCurrency);
	toCurrency = encodeURIComponent(toCurrency);
	var query = fromCurrency + '_' + toCurrency;

  	var url = 'https://free.currencyconverterapi.com/api/v6/convert?q='
            + query + '&compact=y';

	var defaultReturnValue = -1;
   var returnValue = defaultReturnValue;
	$.getJSON(url, function(data) {
		if(data != null){
			var convertion = data[query]["val"] * amount;
			returnValue = convertion;
			printConvertion(toCurrency,returnValue);
		}
 	});
	return returnValue;
};
function convertCurrencies(callingInput){
	if(isNaN(callingInput.value)){
		return false;
	}else{
		try {
			var value = parseFloat(callingInput.value);
			var elements = document.getElementById("formFavorites_currencies").getElementsByTagName("li");
			//var elements = document.getElementById("formFavorites_currencies").elements;
			for(var i = 0, element; element = elements[i++];){
				if(element.className === "show"){
					var inputs = document.getElementById(element.id).getElementsByTagName("input");
					for(var j = 0, input; input = inputs[j++];){
						if(input.id !== callingInput.id){
							convertCurrency(value,callingInput.id,input.id);
						}
					}
				}
			}
		} catch (e) {
			console.log(e);
			alert('Error');
		}
	}

}



function printConvertion(toCurrency,convertion){
	var currencyGoalLabel = toCurrency;
	currencyGoalLabel += "-con";
	document.getElementById(currencyGoalLabel).innerHTML = convertion;
};
